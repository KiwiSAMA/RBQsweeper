import scipy.signal
class RBQCounter(object):
    def counter(self,rbqMatrix):
        rbqMatrix=scipy.signal.convolve2d(rbqMatrix,[[1,1,1],[1,0,1],[1,1,1]],'same')
        return rbqMatrix
