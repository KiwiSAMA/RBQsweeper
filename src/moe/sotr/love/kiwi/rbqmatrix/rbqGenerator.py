import numpy as np
import scipy as sp
import random


class RBQgenerator(object):
    def __init__(self):
        pass

    def generator(self, column=10, row=10, rbqnumber='random'):
        rbqMatrix = np.random.randint(0, 2, (column, row))
        # limited Matrix size and random RBQ number
        generatedrbqdnumber = (sum(sum(rbqMatrix)))
        # count RBQ number
        return rbqMatrix, generatedrbqdnumber
        # return RBQ Matrix and RBQ number

    def generator2(self, column=10, row=10, rbqnumber=50):
        # DONT USE THIS
        # JUST SIMPLE FOR THINKING AND WRITING
        while True:
            rbqMatrix = np.random.randint(0, 2, (column, row))
            generatedRBQnumber = (sum(sum(rbqMatrix)))
            # count RBQ number
            if int(generatedRBQnumber) == int(rbqnumber):
                break
        return rbqMatrix
        # return RBQ Matrix

    def generator3(self, column=10, row=10, rbqnumber=50):
        rbqMatrix = np.zeros(shape=(column, row), dtype=int)
        # generate a matrix with all-zero with a limited sized
        randomRBQ = random.sample(range(column*row), rbqnumber)
        # generate different number in max size range
        for RBQ in randomRBQ:
            rbqMatrix[(RBQ-1)//row][(RBQ-1) % column] = 1
            # replace randomRBQ's number into Matrix
        return rbqMatrix, randomRBQ
        # return RBQ Matrix

# print(RBQgenerator_.generator(None,10,10))
# print(RBQgenerator_.generator2(None,10,10,50))
# print(RBQgenerator_.generator3(None, 10, 10, 20))
