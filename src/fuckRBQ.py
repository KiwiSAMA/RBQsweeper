import moe.sotr.love.kiwi.rbqmatrix.rbqGenerator
import moe.sotr.love.kiwi.rbqmatrix.rbqCounter


generate = moe.sotr.love.kiwi.rbqmatrix.rbqGenerator.RBQgenerator.generator3
count = moe.sotr.love.kiwi.rbqmatrix.rbqCounter.RBQCounter.counter


tablecolumn = 10
tablerow = 10
tablerbqnumber = 50

rbqMatrix = generate(None, column=tablecolumn,
                     row=tablerow, rbqnumber=tablerbqnumber)
print(rbqMatrix[0])
rbqcount = count(None,rbqMatrix[0])
print(rbqcount)